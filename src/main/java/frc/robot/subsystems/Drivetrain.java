/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.sensors.CANCoder;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.DrivetrainConstants;

public class Drivetrain extends SubsystemBase {
  /**
   * Creates a new Drivetrain.
   */

  private TalonFX leftFrontTurn;
  private TalonFX leftBackTurn;
  private TalonFX rightFrontTurn;
  private TalonFX rightBackTurn;

  private TalonFX leftFrontDrive;
  private TalonFX leftBackDrive;
  private TalonFX rightFrontDrive;
  private TalonFX rightBackDrive;

  private CANCoder leftFrontEncoder;
  private CANCoder leftBackEncoder;
  private CANCoder rightFrontEncoder;
  private CANCoder rightBackEncoder;

  private AHRS navX;

  private SwerveDriveKinematics kinematics;
  private SwerveModule[] modules;


  public static final double kMaxSpeed = Units.feetToMeters(13.6); // 13.6 feet per second

  private boolean fieldRelative;


  public Drivetrain() {

    // copied from frc swerve example

    /**
     * 
     * Modules are in the order of -
     * Front Left
     * Front Right
     * Back Left
     * Back Right
     * 
     * Measurements are relative to the center of the robot
     * 
     * Positive x values represent moving toward the front of the robot whereas
     * positive y values represent moving toward the left of the robot
     * https://docs.wpilib.org/en/stable/docs/software/kinematics-and-odometry/swerve-drive-kinematics.html#constructing-the-kinematics-object
     */
    kinematics = new SwerveDriveKinematics(
      new Translation2d(
        Units.inchesToMeters(12),
        Units.inchesToMeters(12)
      ),
      new Translation2d(
        Units.inchesToMeters(12),
        Units.inchesToMeters(-12)
      ),
      new Translation2d(
        Units.inchesToMeters(-12),
        Units.inchesToMeters(12)
      ),
      new Translation2d(
        Units.inchesToMeters(-12),
        Units.inchesToMeters(-12)
      )
    );

    navX = new AHRS(SPI.Port.kMXP);
    navX.zeroYaw();

    leftFrontTurn = new TalonFX(DrivetrainConstants.LEFT_FRONT_TURN);
    leftBackTurn = new TalonFX(DrivetrainConstants.LEFT_BACK_TURN);
    rightFrontTurn = new TalonFX(DrivetrainConstants.RIGHT_FRONT_TURN);
    rightBackTurn = new TalonFX(DrivetrainConstants.RIGHT_BACK_TURN);

    leftFrontDrive = new TalonFX(DrivetrainConstants.LEFT_FRONT_DRIVE);
    leftBackDrive = new TalonFX(DrivetrainConstants.LEFT_BACK_DRIVE);
    rightFrontDrive = new TalonFX(DrivetrainConstants.RIGHT_FRONT_DRIVE);
    rightBackDrive = new TalonFX(DrivetrainConstants.RIGHT_BACK_DRIVE);

    leftFrontEncoder = new CANCoder(DrivetrainConstants.LEFT_FRONT_ENCODER);
    leftBackEncoder = new CANCoder(DrivetrainConstants.LEFT_BACK_ENCODER);
    rightFrontEncoder = new CANCoder(DrivetrainConstants.RIGHT_FRONT_ENCODER);
    rightBackEncoder = new CANCoder(DrivetrainConstants.RIGHT_BACK_ENCODER);

    // Edit headings (the final argument in the constructor) to match the offsets of the absolute encoder positions on the robot
    modules = new SwerveModule[] {
      new SwerveModule(leftFrontDrive, leftFrontTurn, leftFrontEncoder, Rotation2d.fromDegrees(67)),  
      new SwerveModule(rightFrontDrive, rightFrontTurn, rightFrontEncoder, Rotation2d.fromDegrees(357)),
      new SwerveModule(leftBackDrive, leftBackTurn, leftBackEncoder, Rotation2d.fromDegrees(39)),
      new SwerveModule(rightBackDrive, rightBackTurn, rightBackEncoder, Rotation2d.fromDegrees(13))
    };

    fieldRelative = true;

    modules[1].setDesiredState(new SwerveModuleState(0, Rotation2d.fromDegrees(90)));
  }

  public void setFieldRelative(boolean fieldRelative) {
    this.fieldRelative = fieldRelative;
  }

  public boolean isFieldRelative() {
    return fieldRelative;
  }

  
  public SwerveDriveKinematics getKinematics() {
    return kinematics;
}

  public void toggleFieldRelative() {
    fieldRelative = !fieldRelative;
  }

  public void swerveDrive(double strafeX, double strafeY, double rotation) {
    SwerveModuleState[] states = kinematics.toSwerveModuleStates(
      fieldRelative
      ? ChassisSpeeds.fromFieldRelativeSpeeds(strafeX, strafeY, rotation, Rotation2d.fromDegrees(-navX.getAngle()))
      : new ChassisSpeeds(strafeX, strafeY, rotation)
    );
    SwerveDriveKinematics.desaturateWheelSpeeds(states, kMaxSpeed);
    for(int i = 0; i < states.length; i++) {
      SwerveModule module = modules[i];
      SwerveModuleState state = states[i];
      module.setDesiredState(state);
    }
  }

  @Override
  public void periodic() {
    SmartDashboard.putNumber("LFE", modules[0].getAngle().getDegrees());
    SmartDashboard.putNumber("RFE", modules[1].getAngle().getDegrees());
    SmartDashboard.putNumber("LBE", modules[2].getAngle().getDegrees());
    SmartDashboard.putNumber("RBE", modules[3].getAngle().getDegrees());
  }
}
