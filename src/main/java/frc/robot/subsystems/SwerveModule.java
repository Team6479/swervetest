package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.RemoteSensorSource;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.can.TalonFXConfiguration;
import com.ctre.phoenix.sensors.CANCoder;
import com.ctre.phoenix.sensors.CANCoderConfiguration;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.util.Units;

public class SwerveModule {

    // TODO: Tune PID values
    private static final double kDriveP = 0;
    private static final double kDriveI = 0;
    private static final double kDriveD = 0;
    private static final double kDriveF = 0;

    public static final double kAngleP = 0.2;
    private static final double kAngleI = 0;
    private static final double kAngleD = 0;

    private TalonFX driveMotor;
    private TalonFX angleMotor;
    private CANCoder canCoder;

    public SwerveModule(TalonFX driveMotor, TalonFX angleMotor, CANCoder canCoder, Rotation2d offset) {
        this.driveMotor = driveMotor;
        this.angleMotor = angleMotor;
        this.canCoder = canCoder;

        TalonFXConfiguration angleTalonConfig = new TalonFXConfiguration();

        angleTalonConfig.slot0.kP = kAngleP;
        angleTalonConfig.slot0.kI = kAngleI;
        angleTalonConfig.slot0.kD = kAngleD;

        angleTalonConfig.remoteFilter0.remoteSensorDeviceID = canCoder.getDeviceID();
        angleTalonConfig.remoteFilter0.remoteSensorSource = RemoteSensorSource.CANCoder;
        angleTalonConfig.primaryPID.selectedFeedbackSensor = FeedbackDevice.RemoteSensor0;
        angleMotor.configAllSettings(angleTalonConfig);

        TalonFXConfiguration driveTalonConfig = new TalonFXConfiguration();

        driveTalonConfig.slot0.kP = kDriveP;
        driveTalonConfig.slot0.kI = kDriveI;
        driveTalonConfig.slot0.kD = kDriveD;
        driveTalonConfig.slot0.kF = kDriveF;
    
        driveMotor.configAllSettings(driveTalonConfig);
    
        CANCoderConfiguration canCoderConfig = new CANCoderConfiguration();
        canCoderConfig.magnetOffsetDegrees = offset.getDegrees();
        canCoder.configAllSettings(canCoderConfig);

    }

    /**
     * Gets the relative rotational position of the module
     * @return The relative rotational position of the angle motor in degrees
     */
    public Rotation2d getAngle() {
        // Note: This assumes the CANCoders are setup with the default feedback coefficient
        // and the sesnor value reports degrees.
        return Rotation2d.fromDegrees(canCoder.getAbsolutePosition());
    }


    /**
     * Minimize the change in heading the desired swerve module state would require by potentially
     * reversing the direction the wheel spins. If this is used with the PIDController class's
     * continuous input functionality, the furthest a wheel will ever rotate is 90 degrees.
     *
     * @param desiredState The desired state.
     * @param currentAngle The current module angle.
     * @return Optimized swerve module state.
     */
    private static SwerveModuleState optimize(SwerveModuleState desiredState, Rotation2d currentAngle) {
        var delta = desiredState.angle.minus(currentAngle);
        if (Math.abs(delta.getDegrees()) > 90.0) {
            return new SwerveModuleState(-desiredState.speedMetersPerSecond, desiredState.angle.rotateBy(Rotation2d.fromDegrees(180.0)));
        } else {
            return new SwerveModuleState(desiredState.speedMetersPerSecond, desiredState.angle);
        }
    }

    /**
     * Set the speed + rotation of the swerve module from a SwerveModuleState object
     * @param desiredState - A SwerveModuleState representing the desired new state of the module
     */
    public void setDesiredState(SwerveModuleState desiredState) {
        Rotation2d currentRotation = getAngle();

        SwerveModuleState state = desiredState;

        // Find the difference between our current rotational position + our new rotational position
        Rotation2d rotationDelta = state.angle.minus(currentRotation);

        // Find the new absolute position of the module based on the difference in rotation
        double deltaTicks = (rotationDelta.getDegrees() / 360) * 4096;
        // Convert the CANCoder from it's position reading back to ticks
        double currentTicks = canCoder.getPosition() / canCoder.configGetFeedbackCoefficient();
        double desiredTicks = currentTicks + deltaTicks;
        angleMotor.set(TalonFXControlMode.Position, desiredTicks);

        double feetPerSecond = Units.metersToFeet(state.speedMetersPerSecond);
        driveMotor.set(TalonFXControlMode.PercentOutput, feetPerSecond / Drivetrain.kMaxSpeed);
    }
}