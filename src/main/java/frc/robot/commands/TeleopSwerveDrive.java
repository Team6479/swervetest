/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class TeleopSwerveDrive extends CommandBase {
  /**
   * Creates a new TeleopSwerveDrive.
   */

  private Drivetrain drivetrain;
  private DoubleSupplier strafeX;
  private DoubleSupplier strafeY;
  private DoubleSupplier rotation;

  public TeleopSwerveDrive(Drivetrain drivetrain, DoubleSupplier strafeX, DoubleSupplier strafeY, DoubleSupplier rotation) {
    this.drivetrain = drivetrain;
    this.strafeX = strafeX;
    this.strafeY = strafeY;
    this.rotation = rotation;

    addRequirements(this.drivetrain);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    drivetrain.swerveDrive(strafeX.getAsDouble(), strafeY.getAsDouble(), rotation.getAsDouble());
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
