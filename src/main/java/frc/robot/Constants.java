/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

    public final class DrivetrainConstants {
        public static final int LEFT_FRONT_DRIVE = 2;
        public static final int LEFT_BACK_DRIVE = 5;
        public static final int RIGHT_FRONT_DRIVE = 4;
        public static final int RIGHT_BACK_DRIVE = 8;

        public static final int LEFT_FRONT_TURN = 7;
        public static final int LEFT_BACK_TURN = 6;
        public static final int RIGHT_FRONT_TURN = 3;
        public static final int RIGHT_BACK_TURN = 1;

        public static final int LEFT_FRONT_ENCODER = 21;
        public static final int LEFT_BACK_ENCODER = 24;
        public static final int RIGHT_FRONT_ENCODER = 22;
        public static final int RIGHT_BACK_ENCODER = 23;
    }
}
