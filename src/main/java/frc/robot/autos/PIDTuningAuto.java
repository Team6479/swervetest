package frc.robot.autos;

import javax.management.InstanceNotFoundException;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.Drivetrain;

public class PIDTuningAuto extends SequentialCommandGroup {

    public PIDTuningAuto(Drivetrain drivetrain) {
        super(
            new WaitCommand(2)
        );
    }
}